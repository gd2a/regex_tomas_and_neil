package SecondSemester;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyRegextesting
{
    public static void main(String[] args)
    {
//        String text = "Hello";
//        text = text.replaceAll("H", "y");
//        System.out.println(text);
//        System.out.println(text.matches("Hello"));// Comes out as False.
//        System.out.println("Next Case Insensitive it works thank holy fuck");
//        System.out.println(text.matches("(?i)(^Yell.)"));// Comes out as true the . is for anything following it. the (?i) makes it case insensitive
//
//
//        String JohnsString = "Hello";
//        System.out.println(JohnsString.matches("^Hell"));//comes out as false as string is Hello not Hell
//        Logicaloperator();
//        AmericanNumber();
//        IrishPhoneNumber();
//        VisaNumbers();
        RegExChallenge();
    }

    public static void Logicaloperator()
    {
        System.out.println("Not Operator = [^]");
        String tvtest = "tstvkt";
        // find all ts that are not followed by a v
        String tNotv = "t[^v]";
        Pattern tNotvPattern = Pattern.compile(tNotv);
        Matcher tNotvMatcher = tNotvPattern.matcher(tvtest);

        int count = 0;
        while (tNotvMatcher.find()) {
            count++;
            System.out.println("Occurrence " + count + " : " + tNotvMatcher.start() + " to " + tNotvMatcher.end());
        }
    }


    public static void AmericanNumber()
    {
        //American phone Number (800) 123-14567
        String Phone1 = "1234567890";
        String Phone2 = "(123) 456-7890";
        String Phone3 = "(123)456-7890";
        String Phone4 = "123 456-7890";

        String PhoneRegEx = "^(\\([0-9]{3}\\)) [0-9]{3}-[0-9]{4}$";
        //  \\ is for the Brackets to interperate them as a bracket
        System.out.println(Phone1.matches("^\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}$"));
        System.out.println(Phone2.matches("^\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}$"));
        System.out.println(Phone3.matches("^\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}$"));
        System.out.println(Phone4.matches("^\\([0-9]{3}\\) [0-9]{3}-[0-9]{4}$"));

    }

    public static void IrishPhoneNumber()
    {
        String IrishRegex = "^[\\+353] [ ]?[8]{1}[0-9]{1} [ ]?[0-9]{3} [ ]? [0-9]{4}";
        String Phone1 = "+353 86 070 8107";
        System.out.println(Phone1.matches("^[\\+353][ ]?[8]{1}[0-9]{1}[ ]?[0-9]{3}[ ]?[0-9]{4}$"));
    }

    public static void VisaNumbers()
    {
        String visa1 = "4444444444444";//true
        String visa2 = "5444444444444";//false
        String visa3 = "4444444444444444";//true
        String visa4 = "4444";//false
        String visa5 = "4444444444444444444";//true

        String visaRegex = "^[4]{1} (\\d{12} | \\d{15} | \\d{18})";
        System.out.println(visa1.matches("^([4]{1})(\\d{12}|\\d{15}|\\d{18})$"));
        System.out.println(visa2.matches("^([4]{1})(\\d{12}|\\d{15}|\\d{18})$"));
        System.out.println(visa3.matches("^([4]{1})(\\d{12}|\\d{15}|\\d{18})$"));
        System.out.println(visa4.matches("^([4]{1})(\\d{12}|\\d{15}|\\d{18})$"));
        System.out.println(visa5.matches("^([4]{1})(\\d{12}|\\d{15}|\\d{18})$"));
    }

    public static void RegExChallenge()
    {
        String challenge1 = "I want a bike.";// match exactly
        System.out.println(challenge1.matches("^I want a bike\\.$"));

        String challenge2 = "I want a ball.";
        System.out.println(challenge1.matches("I want a .+\\.$"));
        System.out.println(challenge2.matches("I want a .+\\.$"));

        //CHallenge 3 do 2 but pattern and matcher.

//        Pattern tNotvPattern = Pattern.compile(tNotv);
//        Matcher tNotvMatcher = tNotvPattern.matcher(tvtest);
//
//        int count = 0;
//        while (tNotvMatcher.find()) {
//            count++;
//            System.out.println("Occurrence " + count + " : " + tNotvMatcher.start() + " to " + tNotvMatcher.end());
//        }

        Pattern ch3pattern = Pattern.compile("I want a .+\\.$");
        Matcher ch3matcher = ch3pattern.matcher(challenge2);
        System.out.println(ch3matcher.matches());

        ch3matcher.reset();

        //Challenge 4
        String challenge4  ="Replace All Blanks with underscores";
        System.out.println(challenge4.replaceAll(" ","_"));

        //Challenge 5
        //only the letters a-g are allowed there has to be atleast one
        String challenge5 = "aaabcccccccccddefffg";
            System.out.println(challenge5.matches("^[a-g]+$"));

            //challenge6
        System.out.println(challenge5.matches("^aaabcccccccccddefffg$"));

        //challenge 7
        String challenge7 = "Abcd.22";
        System.out.println(challenge7.matches("^(?i)([a-z]+\\.\\d+$)"));

        //challenge 8
        String challenge8 = "abcd.135uvqz.7tzik.999";
        Pattern ch8Pattern =  Pattern.compile("[a-z]+\\.(\\d+)+");
        Matcher ch8matcher = ch8Pattern.matcher(challenge8);
        int count = 0;
        while(ch8matcher.find())
        {
            count++;
            System.out.println("Occurrence " + count + " : " + ch8matcher.start() + " to " + ch8matcher.end());
            System.out.println(ch8matcher.group(1));
        }



        /* Challenge 9: Suppose you are reading strings that match patterns
        used in challenges 7 and 8 from a file. Tabs are used
        to separate the matches with one exception. The last
        match is followed by a newline
         */
        String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
        //Revise the regular expression and extract all the numbers

        Pattern ch9Pattern =  Pattern.compile("([a-z]+\\.(\\d+))\\s+");
        Matcher ch9matcher = ch9Pattern.matcher(challenge9);
        count = 0;
        while(ch9matcher.find())
        {
            count++;
            System.out.println("Occurrence " + count + " : " + ch9matcher.start() + " to " + ch9matcher.end());
            System.out.println(ch9matcher.group(2));
        }

        //Round 2
        /*Challenge 10 Instead of printing out the numbers
        print out their start and end indices. Use the same string as in 9. Make the indices inclusive e.g. start index printed for 135 would be 5 and the end should be 7
        Hint: Look at the documentation for Matcher.start() and Matcher.end()
         */

        /* Challenge 11 Suppose we have the following string containing points on a graph within curly braces. Extract what is in the curly braces*/
        String challenge11 = "{0, 2}, {0, 5}, {1, 3}, {2, 4}";

        Pattern ch11Pattern =  Pattern.compile("[{](\\d\\, \\d)[}][,]?");
        Matcher ch11matcher = ch11Pattern.matcher(challenge11);
        count = 0;
        System.out.println("Challenge 11");
        while(ch11matcher.find())
        {
            count++;
            System.out.println("Occurrence " + count + " : " + ch11matcher.start() + " to " + ch11matcher.end());
            System.out.println(ch11matcher.group(1));
        }


        /* Challenge 12 Write a regex to match a 5 digit US zip code
         */
        String challenge12 = "11111";

        Pattern ch12Pattern =  Pattern.compile("\\d{5}");
        Matcher ch12matcher = ch12Pattern.matcher(challenge12);
        System.out.println("Challenge 12");
        System.out.println(ch12matcher.matches());

        /*Challenge 13 US zip codes can ve followed by a dash and another four numbers. Write regex for those zip codes*/
        String challenge13 = "11111-1111";

        Pattern ch13Pattern =  Pattern.compile("\\d{5}-\\d{4}");
        Matcher ch13matcher = ch13Pattern.matcher(challenge13);
        System.out.println("Challenge 13");
        System.out.println(ch13matcher.matches());


        /*Challenge14 Write a regex that matches both challenge 12 and 13*/
        Pattern ch14Pattern =  Pattern.compile("(\\d{5}-\\d{4}|\\d{5})");
        Matcher ch14matcher = ch14Pattern.matcher(challenge13);
        System.out.println("Challenge 14");
        System.out.println(ch14matcher.matches());



    }
}